"use strict";
const Generator = require("yeoman-generator");
const chalk = require("chalk");
const yosay = require("yosay");

module.exports = class extends Generator {
  prompting() {
    // Have Yeoman greet the user.
    this.log(
      yosay(`Welcome to the unreal ${chalk.red("generator-rc")} generator!`)
    );

    const prompts = [
      {
        type: "confirm",
        name: "someAnswer",
        message: "Would you like to enable this option?",
        default: true
      }
    ];

    return this.prompt(prompts).then(props => {
      // To access props later use this.props.someAnswer;
      this.props = props;
    });
  }

  _writingDot() {
    this.fs.copy(this.templatePath(".*"), this.destinationPath());
  }

  _writingPackage() {
    this.fs.copy(
      this.templatePath("package.json"),
      this.destinationPath("package.json")
    );
  }

  _writingReadME() {
    this.fs.copy(
      this.templatePath("README.md"),
      this.destinationPath("README.md")
    );
  }

  _writingTs() {
    this.fs.copy(
      this.templatePath("tsconfig.json"),
      this.destinationPath("tsconfig.json")
    );
  }

  _writingOver() {
    this.fs.copy(
      this.templatePath("config-overrides.js"),
      this.destinationPath("config-overrides.js")
    );
  }

  _writingSrc() {
    this.fs.copy(this.templatePath("src/*"), this.destinationPath("src"));
    this.fs.copy(
      this.templatePath("src/options/*"),
      this.destinationPath("src/options")
    );
    this.fs.copy(
      this.templatePath("src/popup/*"),
      this.destinationPath("src/popup")
    );
  }

  _writingPub() {
    this.fs.copy(this.templatePath("public/*"), this.destinationPath("public"));
  }

  writing() {
    this._writingDot();
    this._writingPackage();
    this._writingReadME();
    this._writingTs();
    this._writingOver();
    this._writingSrc();
    this._writingPub();
  }

  install() {
    this.installDependencies({ bower: false });
  }
};
