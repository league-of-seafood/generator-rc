const {
    override,
    overrideDevServer,
    addWebpackPlugin,
    addWebpackModuleRule,
    fixBabelImports
} = require("customize-cra");
const CopyPlugin = require('copy-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const getCSSModuleLocalIdent = require('react-dev-utils/getCSSModuleLocalIdent');
const getPublicUrlOrPath = require('react-dev-utils/getPublicUrlOrPath');
const postcssNormalize = require('postcss-normalize');
const path = require('path');
const fs = require('fs');

const shouldUseSourceMap = process.env.GENERATE_SOURCEMAP !== 'false';
const isEnvDevelopment = process.env.NODE_ENV === 'development';
const isEnvProduction = process.env.NODE_ENV === 'production';

const lessRegex = /\.less$/
const lessModuleRegex = /\.module\.less$/

const appDirectory = fs.realpathSync(process.cwd());

const resolveApp = relativePath => path.resolve(appDirectory, relativePath);

const publicUrlOrPath = getPublicUrlOrPath(
    process.env.NODE_ENV === 'development',
    require(resolveApp('package.json')).homepage,
    process.env.PUBLIC_URL
);

const getStyleLoaders = (cssOptions, preProcessor) => {
    const loaders = [
        isEnvDevelopment && require.resolve('style-loader'),
        isEnvProduction && {
            loader: MiniCssExtractPlugin.loader,
            // css is located in `static/css`, use '../../' to locate index.html folder
            // in production `paths.publicUrlOrPath` can be a relative path
            options: publicUrlOrPath.startsWith('.')
                ? { publicPath: '../../' }
                : {},
        },
        {
            loader: require.resolve('css-loader'),
            options: cssOptions,
        },
        {
            // Options for PostCSS as we reference these options twice
            // Adds vendor prefixing based on your specified browser support in
            // package.json
            loader: require.resolve('postcss-loader'),
            options: {
                // Necessary for external CSS imports to work
                // https://github.com/facebook/create-react-app/issues/2677
                ident: 'postcss',
                plugins: () => [
                    require('postcss-flexbugs-fixes'),
                    require('postcss-preset-env')({
                        autoprefixer: {
                            flexbox: 'no-2009',
                        },
                        stage: 3,
                    }),
                    // Adds PostCSS Normalize as the reset css with default options,
                    // so that it honors browserslist config in package.json
                    // which in turn let's users customize the target behavior as per their needs.
                    postcssNormalize(),
                ],
                sourceMap: isEnvProduction && shouldUseSourceMap,
            },
        },
    ].filter(Boolean);
    if (preProcessor) {
        loaders.push(
            {
                loader: require.resolve('resolve-url-loader'),
                options: {
                    sourceMap: isEnvProduction && shouldUseSourceMap,
                },
            }
        );
        if (preProcessor == 'less-loader') {
            loaders.push({
                loader: require.resolve(preProcessor),
                options: {
                    sourceMap: true,
                    lessOptions: {
                        javascriptEnabled: true,
                        modifyVars: {
                            'primary-color': '#E6879B'
                        }
                    },
                },
            })
        } else {
            loaders.push({
                loader: require.resolve(preProcessor),
                options: {
                    sourceMap: true
                },
            })
        }
    }
    return loaders;
};

const lessModuleRule = {
    test: lessModuleRegex,
    use: getStyleLoaders(
        {
            importLoaders: 3,
            sourceMap: isEnvProduction && shouldUseSourceMap,
            modules: {
                getLocalIdent: getCSSModuleLocalIdent
            }
        },
        'less-loader'
    )
}
const lessRule = {
    test: lessRegex,
    exclude: lessModuleRegex,
    use: getStyleLoaders(
        {
            importLoaders: 3,
            sourceMap: isEnvProduction && shouldUseSourceMap
        },
        'less-loader'
    ),
    sideEffects: true
}

const multipleEntry = require('react-app-rewire-multiple-entry')([
    {
        // points to the popup entry point
        entry: 'src/popup/index.tsx',
        template: 'public/popup.html',
        outPath: '/popup.html'
    },
    {
        // points to the options page entry point
        entry: 'src/options/index.tsx',
        template: 'public/index.html',
        outPath: '/index.html'
    }
]);

const devServerConfig = () => config => {
    console.log(config)
    return {
        ...config,
        // webpackDevService doesn't write the files to desk
        // so we need to tell it to do so so we can load the
        // extension with chrome
        writeToDisk: true
    }
}

const copyPlugin = new CopyPlugin({
    patterns: [
        // copy assets
        // { from: 'public', to: '' },
        { from: 'src/background.js', to: '' },
        { from: 'public/favicon.ico', to: '' },
        { from: 'public/logo192.png', to: '' },
        { from: 'public/manifest.json', to: '' }
    ]
})

module.exports = {
    webpack: override(
        addWebpackPlugin(
            copyPlugin
        ),
        addWebpackModuleRule(lessModuleRule),
        addWebpackModuleRule(lessRule),
        fixBabelImports('import', {
            libraryName: 'antd',
            style: true,
        }),
        multipleEntry.addMultiEntry,
    ),
    devServer: overrideDevServer(
        devServerConfig()
    ),

};